package com.mix.farmville.controllers;

import com.mix.farmville.models.payloads.account.AccountInfo;
import com.mix.farmville.services.data.AccountService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
class AccountControllerTest {

    private MockMvc mockMvc;
    @InjectMocks
    private AccountController accountController;
    @Mock
    private AccountService accountService;

    private List<AccountInfo> accounts;

    private AccountInfo accountInfo;

    @BeforeEach
    public void init() {
        mockMvc = MockMvcBuilders
                .standaloneSetup(accountController)
                .build();

        accountInfo = new AccountInfo();
        accountInfo.setFunds(2214.5);
        accountInfo.setAccountNumber(32);

        accounts = new ArrayList<>();
        accounts.add(accountInfo);
        accounts.add(accountInfo);
    }

    @Test
    @DisplayName("Fetch Accounts")
    void getAccounts() throws Exception {
        when(accountService.findAll()).thenReturn(accounts);

        mockMvc.perform(get("/accounts"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()", is(accounts.size())));

        verify(accountService, times(1)).findAll();
    }

//    @Test
//    @DisplayName("Fetch User Available Accounts")
//    void getUserAvailableAccounts() throws Exception {
//        String email = "email@email.email";
//        when(accountService.findUserAvailableAccounts(email)).thenReturn(accounts);
//
//        mockMvc.perform(get("/accounts/available"))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.size()", is(accounts.size())));
//
//        verify(accountService, times(1)).findUserAvailableAccounts(email);
//    }

    @Test
    @DisplayName("Fetch Account")
    void getAccount() throws Exception {
        Integer id = 1;
        when(accountService.findById(id)).thenReturn(accountInfo);

        mockMvc.perform(get("/accounts?id=" + id))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.funds", is(accountInfo.getFunds())));

        verify(accountService, times(1)).findById(id);
    }

//    @Test
//    @DisplayName("Grant Access")
//    void grantAccountAccessToUser() throws Exception {
//        int accountId = 1;
//        int userId = 1;
//        mockMvc.perform(post("/accounts/grant?id=" + accountId + "&user=" + userId))
//                .andExpect(status().isOk());
//        verify(accountService, times(1)).grantAccess(accountId, userId);
//    }
//
//    @Test
//    @DisplayName("Revoke Access")
//    void revokeAccountAccessFromUser() throws Exception {
//        int accountId = 1;
//        int userId = 1;
//        mockMvc.perform(post("/accounts/revoke?id=" + accountId + "&user=" + userId))
//                .andExpect(status().isOk());
//        verify(accountService, times(1)).revokeAccess(accountId, userId);
//    }
}