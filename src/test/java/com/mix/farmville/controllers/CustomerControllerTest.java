package com.mix.farmville.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mix.farmville.models.payloads.customer.CustomerInfo;
import com.mix.farmville.models.payloads.customer.MutateCustomer;
import com.mix.farmville.services.data.CustomerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
class CustomerControllerTest {

    private MockMvc mockMvc;
    @Mock
    private CustomerService customerService;
    @InjectMocks
    private CustomerController customerController;

    private List<CustomerInfo> customers;
    private CustomerInfo customerInfo;
    private MutateCustomer mutateCustomer;
    private final ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    public void init() {
        mockMvc = MockMvcBuilders
                .standaloneSetup(customerController)
                .build();
        customerInfo = createCustomerInfo();

        mutateCustomer = createMutateCustomer();

        customers = new ArrayList<>();
        customers.add(customerInfo);
        customers.add(customerInfo);
    }

    @Test
    @DisplayName("Fetch Customers")
    void getCustomers() throws Exception {
        when(customerService.findAll()).thenReturn(customers);

        mockMvc.perform(get("/customers"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()", is(customers.size())));

        verify(customerService, times(1)).findAll();
    }

    @Test
    @DisplayName("Fetch Customer")
    void getCustomer() throws Exception {
        int id = 1;
        when(customerService.findById(id)).thenReturn(customerInfo);

        mockMvc.perform(get("/customers")
        .queryParam("id", id+""))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName", is("Customer")));
        verify(customerService, times(1)).findById(id);
    }

    @Test
    @DisplayName("Create Customer")
    void createCustomer() throws Exception {
        when(customerService.createCustomer(mutateCustomer)).thenReturn(customerInfo);

        mockMvc.perform(post("/customers")
                .content(objectMapper.writeValueAsString(mutateCustomer))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.lastName", is("Customer")));

        verify(customerService, times(1)).createCustomer(mutateCustomer);
    }
    @Test
    @DisplayName("Create Customer Fail")
    void expectFailCreateCustomer() throws Exception {
        MutateCustomer invalidCustomer = createMutateCustomer();
        invalidCustomer.setEmail("");

        mockMvc.perform(post("/customers")
                .content(objectMapper.writeValueAsString(invalidCustomer))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        verify(customerService, times(0)).createCustomer(mutateCustomer);
    }

    @Test
    @DisplayName("Update Customer")
    void updateCustomer() throws Exception {
        int id = 1;
        when(customerService.updateCustomer(id, mutateCustomer)).thenReturn(customerInfo);

        mockMvc.perform(patch("/customers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(mutateCustomer))
                .queryParam("id", id + ""))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(id)));

        verify(customerService, times(1)).updateCustomer(id, mutateCustomer);
    }

    @Test
    @DisplayName("Delete Customer")
    void deleteCustomer() throws Exception {
        int id = 1;
        mockMvc.perform(delete("/customers")
                .queryParam("id", id + ""))
                .andExpect(status().isOk());

        verify(customerService, times(1)).deleteById(id);
    }

    private CustomerInfo createCustomerInfo() {
        CustomerInfo customerInfo = new CustomerInfo();
        customerInfo.setAddress("Address");
        customerInfo.setEmail("test@test.test");
        customerInfo.setFirstName("Customer");
        customerInfo.setLastName("Customer");
        customerInfo.setId(1);
        customerInfo.setPhone("+111/88-55-22-11");
        return customerInfo;
    }

    private MutateCustomer createMutateCustomer() {
        MutateCustomer mutateCustomer = new MutateCustomer();
        mutateCustomer.setAddress("Address");
        mutateCustomer.setEmail("test@test.test");
        mutateCustomer.setFirstName("Customer");
        mutateCustomer.setLastName("Customer");
        mutateCustomer.setPhone("+111/88-55-22-11");
        return mutateCustomer;
    }
}