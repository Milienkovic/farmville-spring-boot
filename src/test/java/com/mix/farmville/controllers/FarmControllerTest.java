package com.mix.farmville.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mix.farmville.models.payloads.farm.FarmInfo;
import com.mix.farmville.models.payloads.farm.MutateFarm;
import com.mix.farmville.services.data.FarmService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ExtendWith(MockitoExtension.class)
class FarmControllerTest {

    private MockMvc mockMvc;


    private final ObjectMapper objectMapper = new ObjectMapper();
    @Mock
    private FarmService farmService;

    @InjectMocks
    private FarmController farmController;

    private List<FarmInfo> farms;
    private FarmInfo farm;
    private MutateFarm mutateFarm;

    @BeforeEach
    public void initData() {
        Integer id = 1;
        farm = new FarmInfo();
        farm.setAddress("Snow Area");
        farm.setId(id);
        farm.setName("Farmville");
        farms = new ArrayList<>();
        farms.add(farm);
        farms.add(farm);
        mutateFarm = new MutateFarm();
        mutateFarm.setAddress("Snow Area");
        mutateFarm.setName("Farmville");
        mockMvc = MockMvcBuilders
                .standaloneSetup(farmController)
                .build();
    }

    @Test
    @DisplayName("Fetch Farms")
    void getFarms() throws Exception {
        when(farmService.findAll()).thenReturn(farms);
        System.out.println(farms.size());
        mockMvc.perform(get("/farms"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()", is(farms.size())));
        verify(farmService, times(1)).findAll();
    }

    @Test
    @DisplayName("Fetch Farm")
    void getFarm() throws Exception {
        Integer id = 1;
        when(farmService.findById(id)).thenReturn(farm);
        mockMvc.perform(get("/farms")
                .queryParam("id", id + ""))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(id)));

        verify(farmService, times(1)).findById(id);
    }

//    @Test
//    @DisplayName("Fetch User Available Farms")
//    void getUserAccessibleFarms() throws Exception {
//        String email = "email@email.email";
//        when(farmService.findUserAvailableFarms(email)).thenReturn(farms);
//        System.out.println(farms.size());
//        mockMvc.perform(get("/farms/available"))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.size()", is(farms.size())));
//        verify(farmService, times(1)).findUserAvailableFarms(email);
//    }

    @Test
    @DisplayName("Create Farm")
    void createFarm() throws Exception {
        Integer id = 1;
        int accountId = 1;
        when(farmService.createFarm(id, mutateFarm)).thenReturn(farm);
        System.err.println(objectMapper.writeValueAsString(mutateFarm));
        mockMvc.perform(post("/farms")
                .content(objectMapper.writeValueAsString(mutateFarm))
                .contentType(MediaType.APPLICATION_JSON)
                .queryParam("account", accountId + ""))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is(id)));
        verify(farmService, times(1)).createFarm(accountId, mutateFarm);
    }

    @Test
    @DisplayName("Update Farm")
    void updateFarm() throws Exception {
        Integer id = 1;
        when(farmService.updateFarm(id, mutateFarm)).thenReturn(farm);

        mockMvc.perform(patch("/farms")
                .queryParam("id", id + "")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(mutateFarm)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(mutateFarm.getName())));
        verify(farmService, times(1)).updateFarm(id, mutateFarm);
    }

    @Test
    @DisplayName("Update Farm Fail")
    void expectFailUpdateFarm() throws Exception {
        Integer id = 1;
        MutateFarm invalidFarm = new MutateFarm();
        invalidFarm.setName("bad name");
        invalidFarm.setAddress("bad address");

        mockMvc.perform(patch("/farms")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(invalidFarm))
                .queryParam("id", id + ""))
                .andExpect(status().isBadRequest())
                .andDo(print());
        verify(farmService, times(0)).updateFarm(id, mutateFarm);
    }

    @Test
    @DisplayName("Delete Farm")
    void deleteFarm() throws Exception {
        int id = 1;
        mockMvc.perform(delete("/farms")
                .queryParam("id", id + ""))
                .andExpect(status().isOk());
        verify(farmService, times(1)).deleteById(id);
    }
}