package com.mix.farmville.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mix.farmville.models.payloads.user.LoginRequest;
import com.mix.farmville.models.payloads.user.RegistrationRequest;
import com.mix.farmville.models.payloads.user.UserInfo;
import com.mix.farmville.services.data.UserService;
import com.mix.farmville.services.security.auth.AuthService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
class AuthControllerTest {

    private MockMvc mockMvc;
    @Mock
    private AuthService authService;
    @Mock
    private UserService userService;
    @InjectMocks
    private AuthController authController;

    private LoginRequest loginRequest;
    private RegistrationRequest registrationRequest;
    private UserInfo userInfo;
    private final ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    public void init() {
        mockMvc = MockMvcBuilders
                .standaloneSetup(authController)
                .build();

        loginRequest = createLoginRequest();
        registrationRequest = createRegistrationRequest();
        userInfo = createUserInfo();
    }

    @Test
    @DisplayName("Login")
    void login() throws Exception {
        when(authService.authUserWithEmailAndPassword(loginRequest)).thenReturn("auth token");
        mockMvc.perform(post("/login")
                .content(objectMapper.writeValueAsString(loginRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(header().exists(HttpHeaders.AUTHORIZATION));

        verify(authService, times(1)).authUserWithEmailAndPassword(loginRequest);
    }
    @Test
    @DisplayName("Login Fail")
    void expectFailLogin() throws Exception {
        LoginRequest invalidLogin = createLoginRequest();
        invalidLogin.setEmail("");
        mockMvc.perform(post("/login")
                .content(objectMapper.writeValueAsString(invalidLogin))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        verify(authService, times(0)).authUserWithEmailAndPassword(invalidLogin);
    }

    @Test
    @DisplayName("Registration")
    void register() throws Exception {
        when(userService.createUser(registrationRequest)).thenReturn(userInfo);
        mockMvc.perform(post("/registration")
                .content(objectMapper.writeValueAsString(registrationRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(header().exists(HttpHeaders.LOCATION))
                .andExpect(jsonPath("$.email", is(registrationRequest.getEmail())));

        verify(userService, times(1)).createUser(registrationRequest);
    }

    @Test
    @DisplayName("Registration Fail")
    void expectRegistrationFail() throws Exception {
        RegistrationRequest invalidRequest = createRegistrationRequest();
        invalidRequest.setConfirmPassword("invalid");
        mockMvc.perform(post("/registration")
                .content(objectMapper.writeValueAsString(invalidRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        verify(userService, times(0)).createUser(invalidRequest);
    }

    private LoginRequest createLoginRequest() {
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setPassword("Pass1");
        loginRequest.setEmail("test@test.test");
        return loginRequest;
    }

    private RegistrationRequest createRegistrationRequest() {
        RegistrationRequest request = new RegistrationRequest();
        request.setPassword("Pass1");
        request.setConfirmPassword("Pass1");
        request.setEmail("test@test.test");

        return request;
    }

    private UserInfo createUserInfo() {
        UserInfo userInfo = new UserInfo();
        userInfo.setEmail("test@test.test");
        userInfo.setId(1);
        userInfo.setFirstName("User");
        userInfo.setLastName("User");
        return userInfo;
    }

}