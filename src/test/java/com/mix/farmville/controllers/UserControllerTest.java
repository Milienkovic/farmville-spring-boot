package com.mix.farmville.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mix.farmville.models.payloads.user.MutateUser;
import com.mix.farmville.models.payloads.user.UserInfo;
import com.mix.farmville.services.data.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
class UserControllerTest {

    private MockMvc mockMvc;

    @Mock
    private UserService userService;
    @InjectMocks
    private UserController userController;

    private List<UserInfo> users;
    private MutateUser mutateUser;
    private UserInfo userInfo;
    private final ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    public void init() {
        mockMvc = MockMvcBuilders
                .standaloneSetup(userController)
                .build();

        mutateUser = createMutateUser();
        userInfo = createUserInfo();
        users = new ArrayList<>();
        users.add(userInfo);
        users.add(userInfo);
    }

    @Test
    @DisplayName("Fetch User")
    void getUser() throws Exception {
        int id = 1;
        when(userService.findById(id)).thenReturn(userInfo);
        mockMvc.perform(get("/users")
                .queryParam("id", id + ""))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName", is("User")));
        verify(userService, times(1)).findById(id);
    }

    @Test
    @DisplayName("Fetch User With Email")
    void testGetUser() throws Exception {
        String email = "test@test.test";
        when(userService.findByEmail(email)).thenReturn(userInfo);
        mockMvc.perform(get("/users")
                .queryParam("email", email))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName", is("User")));

        verify(userService, times(1)).findByEmail(email);
    }

    @Test
    @DisplayName("Fetch Users")
    void getUsers() throws Exception {
        when(userService.findAll()).thenReturn(users);
        mockMvc.perform(get("/users"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()", is(users.size())));

        verify(userService, times(1)).findAll();
    }

    @Test
    @DisplayName("Update User")
    void updateUser() throws Exception {
        int id = 1;
        when(userService.updateUser(id, mutateUser)).thenReturn(userInfo);

        mockMvc.perform(patch("/users")
                .queryParam("id", id + "")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(mutateUser)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(id)));

        verify(userService, times(1)).updateUser(id, mutateUser);
    }

    @Test
    @DisplayName("Update User Fail")
    void expectFailUpdateUser() throws Exception {
        int id = 1;
        MutateUser invalidUser = createMutateUser();
        invalidUser.setAddress("");
        mockMvc.perform(patch("/users")
                .queryParam("id", id + "")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(invalidUser)))
                .andExpect(status().isBadRequest());

        verify(userService, times(0)).updateUser(id, invalidUser);
    }

    @Test
    @DisplayName("Delete User")
    void deleteUser() throws Exception {
        int id = 1;
        String email = "test@test.test";
//        mockMvc.perform(delete("/users")
//                .queryParam("id", id + ""))
//                .andExpect(status().isOk());
//        verify(userService, times(1)).deleteUser(email);
    }

    private UserInfo createUserInfo() {
        UserInfo userInfo = new UserInfo();
        userInfo.setAddress("Address");
        userInfo.setEmail("test@test.test");
        userInfo.setFirstName("User");
        userInfo.setLastName("User");
        userInfo.setPhone("111815");
        userInfo.setId(1);
        return userInfo;
    }

    private MutateUser createMutateUser() {
        MutateUser mutateUser = new MutateUser();
        mutateUser.setAddress("Address");
        mutateUser.setFirstName("User");
        mutateUser.setLastName("User");
        mutateUser.setPhone("111815");
        return mutateUser;
    }
}