package com.mix.farmville.repos;

import com.mix.farmville.models.entities.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CustomerRepo extends JpaRepository<Customer, Integer> {
    Optional<Customer> findById(Integer id);
    Optional<Customer> findByEmail(String email);
    boolean existsByEmail(String email);

}
