package com.mix.farmville.repos;

import com.mix.farmville.models.entities.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AccountRepo extends JpaRepository<Account, Integer> {

    @Modifying(flushAutomatically = true, clearAutomatically = true)
    @Query(value = "delete from users_accounts where users_accounts.account_id = :accountId", nativeQuery = true)
    void removeAccountFromUsers(@Param("accountId") Integer accountId);

    boolean existsByAccountNumber(Integer accountNumber);
    Account findByAccountNumber(Integer accountNumber);
}
