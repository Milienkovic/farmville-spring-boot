package com.mix.farmville.repos;

import com.mix.farmville.models.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface UserRepo extends JpaRepository<User, Integer> {
    Optional<User> findByEmail(String email);

    Optional<User> findById(Integer id);

    @Modifying(flushAutomatically = true, clearAutomatically = true)
    @Query(value = "delete from `users_accounts` where users_accounts.user_id = :userId", nativeQuery = true)
    void removeAccountAccessFromUser(@Param("userId") Integer userId);

    boolean existsByEmail(String email);
    void deleteByEmail(String email);
}
