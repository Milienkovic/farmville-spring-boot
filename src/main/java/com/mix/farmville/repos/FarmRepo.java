package com.mix.farmville.repos;

import com.mix.farmville.models.entities.Farm;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;
import java.util.stream.Stream;

public interface FarmRepo extends JpaRepository<Farm, Integer> {

    @Query(value = "select * from farms where farms.account_id in ( select account_id from users_accounts as ua where ua.user_id=:userId) order by farms.account_id", nativeQuery = true)
    Stream<Farm> findUserAccessibleFarms(@Param("userId") Integer userId);
    boolean existsByName(String name);
    Optional<Farm> findByName(String name);
    Stream<Farm> findAllByAccountId(Integer accountId);
}
