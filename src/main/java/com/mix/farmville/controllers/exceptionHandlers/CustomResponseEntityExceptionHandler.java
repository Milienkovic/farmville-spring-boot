package com.mix.farmville.controllers.exceptionHandlers;

import com.mix.farmville.models.payloads.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@ControllerAdvice
public class CustomResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ErrorResponse response = composeResponse(ex.getBindingResult(), status, request);
        return ResponseEntity.status(status).body(response);
    }

    @Override
    protected ResponseEntity<Object> handleBindException(BindException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ErrorResponse response = composeResponse(ex, status, request);
        return ResponseEntity.status(status).body(response);
    }

    private ErrorResponse composeResponse(BindingResult result, HttpStatus status, WebRequest request) {
        ErrorResponse response = new ErrorResponse();
        log.error(((ServletWebRequest) request).getRequest().getServletPath());
        response.setPath(((ServletWebRequest) request).getRequest().getServletPath());
        if (result.hasGlobalErrors()) {
            response.setMessage(result.getGlobalError().getDefaultMessage());
        }
        response.setFieldErrors(getFieldErrors(result.getFieldErrors()));
        response.setStatus(status.value());
        response.setError(status.name());
        response.setTimestamp(new Timestamp(System.currentTimeMillis()));
        return response;
    }

    private Map<String, String> getFieldErrors(List<FieldError> fieldErrors) {
        Map<String, String> errors = new HashMap<>();
        for (FieldError fieldError : fieldErrors) {
            errors.put(fieldError.getField(), fieldError.getDefaultMessage());
        }
        return errors;
    }
}
