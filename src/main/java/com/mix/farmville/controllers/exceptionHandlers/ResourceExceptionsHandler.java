package com.mix.farmville.controllers.exceptionHandlers;

import com.mix.farmville.exceptions.DuplicateEntryException;
import com.mix.farmville.exceptions.ResourceNotFoundException;
import com.mix.farmville.models.payloads.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;

@Slf4j
@ControllerAdvice
public class ResourceExceptionsHandler {

    private HttpServletRequest request;

    @Autowired
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<ErrorResponse> handleResourceNotFoundException(ResourceNotFoundException ex) {
        ErrorResponse response = createResponse(ex, HttpStatus.NOT_FOUND);
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    }

    @ExceptionHandler(DuplicateEntryException.class)
    public ResponseEntity<ErrorResponse> handleDuplicateEntry(DuplicateEntryException ex){
        ErrorResponse response = createResponse(ex, HttpStatus.CONFLICT);
        return ResponseEntity.status(HttpStatus.CONFLICT).body(response);
    }

    private ErrorResponse createResponse(RuntimeException ex, HttpStatus status){
        ErrorResponse response = new ErrorResponse();
        response.setTimestamp(new Timestamp(System.currentTimeMillis()));
        response.setMessage(ex.getLocalizedMessage());
        response.setError(status.getReasonPhrase());
        response.setStatus(status.value());
        response.setPath(request.getServletPath());
        return response;
    }
}
