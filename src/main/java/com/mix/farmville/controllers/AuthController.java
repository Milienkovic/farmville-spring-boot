package com.mix.farmville.controllers;

import com.mix.farmville.models.payloads.user.LoginRequest;
import com.mix.farmville.models.payloads.user.RegistrationRequest;
import com.mix.farmville.models.payloads.user.UserInfo;
import com.mix.farmville.services.data.UserService;
import com.mix.farmville.services.security.auth.AuthService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.URI;

@RestController
public class AuthController {

    private final UserService userService;
    private final AuthService authService;

    public AuthController(UserService userService, AuthService authService) {
        this.userService = userService;
        this.authService = authService;
    }

    @PostMapping(value = "/login")
    public ResponseEntity<?> login(@RequestBody @Valid LoginRequest loginRequest) {
        String token = authService.authUserWithEmailAndPassword(loginRequest);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.AUTHORIZATION, token);
        return ResponseEntity.status(HttpStatus.OK)
                .headers(httpHeaders)
                .body("You have been logged in");
    }

    @PostMapping(value = "/registration")
    public ResponseEntity<?> register(@RequestBody @Valid RegistrationRequest registrationRequest,
                                      HttpServletRequest request) {
        String path = "http://" + request.getServerName() + ":" + request.getServerPort() + "/users";
        UserInfo userInfo = userService.createUser(registrationRequest);
        URI location = ServletUriComponentsBuilder
                .fromHttpUrl(path)
                .queryParam("id", userInfo.getId())
                .buildAndExpand(userInfo.getId())
                .toUri();
        return ResponseEntity.created(location).body(userInfo);
    }
}
