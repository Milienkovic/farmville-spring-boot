package com.mix.farmville.controllers;

import com.mix.farmville.models.payloads.customer.CustomerInfo;
import com.mix.farmville.models.payloads.customer.MutateCustomer;
import com.mix.farmville.services.data.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@Slf4j
@RestController
public class CustomerController {
    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/customers")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<List<CustomerInfo>> getCustomers() {
        return ResponseEntity.ok(customerService.findAll());
    }

    @GetMapping(value = "/customers", params = "id")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<CustomerInfo> getCustomer(@RequestParam("id") Integer customerId) {
        return ResponseEntity.ok(customerService.findById(customerId));
    }

    @PostMapping(value = "/customers")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<CustomerInfo> createCustomer(@RequestBody @Valid MutateCustomer mutateCustomer,
                                                       HttpServletRequest request) {
        log.error(mutateCustomer +"");
        CustomerInfo customerInfo = customerService.createCustomer(mutateCustomer);
        String url = "http://" + request.getServerName() + ":" + request.getServerPort() + "/customers";
        URI location = ServletUriComponentsBuilder
                .fromHttpUrl(url)
                .queryParam("id", customerInfo.getId())
                .buildAndExpand(customerInfo.getId())
                .toUri();
        return ResponseEntity.created(location).body(customerInfo);
    }

    @PatchMapping(value = "/customers", params = "id")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<CustomerInfo> updateCustomer(@RequestParam("id") Integer customerId,
                                                       @RequestBody @Valid MutateCustomer mutateCustomer) {
        return ResponseEntity.ok(customerService.updateCustomer(customerId, mutateCustomer));
    }

    @DeleteMapping(value = "/customers", params = "id")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> deleteCustomer(@RequestParam("id") Integer customerId) {
        customerService.deleteById(customerId);
        return ResponseEntity.ok("Customer has been deleted");
    }
}
