package com.mix.farmville.controllers;

import com.mix.farmville.models.payloads.farm.FarmInfo;
import com.mix.farmville.models.payloads.farm.MutateFarm;
import com.mix.farmville.services.data.FarmService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.URI;
import java.security.Principal;
import java.util.List;

@RestController
public class FarmController {

    private final FarmService farmService;

    public FarmController(FarmService farmService) {
        this.farmService = farmService;
    }

    @GetMapping(value = "/farms", params = "id")
    public ResponseEntity<FarmInfo> getFarm(@RequestParam("id") Integer farmId) {
        return ResponseEntity.ok(farmService.findById(farmId));
    }

    @GetMapping("/farms")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<List<FarmInfo>> getFarms() {
        return ResponseEntity.ok(farmService.findAll());
    }

    @GetMapping(value = "/farms/available")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<List<FarmInfo>> getUserAccessibleFarms(Principal principal){
        return ResponseEntity.ok(farmService.findUserAvailableFarms(principal.getName()));
    }
    @GetMapping(value = "/farms", params = "account")
    public ResponseEntity<List<FarmInfo>> getAccountFarms(@RequestParam("account") Integer accountId){
        return ResponseEntity.ok(farmService.findAccountFarms(accountId));
    }

    @PostMapping(value = "/farms", params = "account")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<FarmInfo> createFarm(@RequestParam("account") Integer accountId,
                                               @RequestBody @Valid MutateFarm mutateFarm,
                                               HttpServletRequest request) {
        FarmInfo farmInfo = farmService.createFarm(accountId, mutateFarm);
        String path = "http://" + request.getServerName() + "/" + request.getServerPort() + "/farms";
        URI location = ServletUriComponentsBuilder
                .fromHttpUrl(path)
                .queryParam("id", farmInfo.getId())
                .buildAndExpand(farmInfo.getId())
                .toUri();
        return ResponseEntity.created(location).body(farmInfo);
    }

    @PatchMapping(value = "/farms", params = "id")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<FarmInfo> updateFarm(@RequestParam("id") Integer farmId,
                                               @RequestBody @Valid MutateFarm mutateFarm) {
        return ResponseEntity.ok(farmService.updateFarm(farmId, mutateFarm));
    }

    @DeleteMapping(value = "/farms", params = "id")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> deleteFarm(@RequestParam("id") Integer farmId){
        farmService.deleteById(farmId);
        return ResponseEntity.ok("Farm has been deleted");
    }
}
