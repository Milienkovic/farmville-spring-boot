package com.mix.farmville.controllers;

import com.mix.farmville.models.payloads.account.AccountInfo;
import com.mix.farmville.services.data.AccountService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
public class AccountController {
    private final AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping("/accounts")
    //for testing purposes
//    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<List<AccountInfo>> getAccounts() {
        return ResponseEntity.ok(accountService.findAll());
    }

    @GetMapping(value = "/accounts/available")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<List<AccountInfo>> getUserAvailableAccounts(Principal principal) {
        return ResponseEntity.ok(accountService.findUserAvailableAccounts(principal.getName()));
    }

    @GetMapping(value = "/accounts", params = "id")
    public ResponseEntity<AccountInfo> getAccount(@RequestParam("id") Integer accountId) {
        return ResponseEntity.ok(accountService.findById(accountId));
    }

    @PostMapping(value = "/accounts/grant", params = {"id"})
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> grantAccountAccessToUser(@RequestParam("id") Integer accountId,
                                                      Principal principal) {
        accountService.grantAccess(accountId, principal.getName());
        return ResponseEntity.ok("Grant request has been sent");
    }

    @PostMapping(value = "/accounts/revoke", params = {"id"})
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> revokeAccountAccessFromUser(@RequestParam("id") Integer accountId,
                                                         Principal principal) {
        accountService.revokeAccess(accountId, principal.getName());
        return ResponseEntity.ok("Revoke request has been sent");
    }
}
