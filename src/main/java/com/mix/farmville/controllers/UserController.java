package com.mix.farmville.controllers;

import com.mix.farmville.models.payloads.user.MutateUser;
import com.mix.farmville.models.payloads.user.UserInfo;
import com.mix.farmville.services.data.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "/users", params = "id")
    public ResponseEntity<UserInfo> getUser(@RequestParam("id") Integer userId){
        UserInfo userInfo = userService.findById(userId);
        return ResponseEntity.ok(userInfo);
    }

    @GetMapping(value = "/users", params = "email")
    public ResponseEntity<UserInfo> getUser(@RequestParam("email") String email){
        UserInfo userInfo = userService.findByEmail(email);
        return ResponseEntity.ok(userInfo);
    }

    @GetMapping("/users")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<List<UserInfo>> getUsers(){
        return ResponseEntity.ok(userService.findAll());
    }

    @PatchMapping(value = "/users", params = "id")
    @PreAuthorize("isAuthenticated() and @userServiceImpl.isMe(#userId, #principal.name)")
    public ResponseEntity<UserInfo> updateUser(@RequestParam("id") Integer userId,
                                               @RequestBody @Valid MutateUser mutateUser,
                                               Principal principal){
        UserInfo userInfo = userService.updateUser(userId, mutateUser);
        return ResponseEntity.ok(userInfo);
    }

    @DeleteMapping(value = "/users", params = "id")
    @PreAuthorize("isAuthenticated() and @userServiceImpl.isMe(#userId, #principal.name)")
    public ResponseEntity<?> deleteUser(@RequestParam("id") Integer userId,
                                        Principal principal){
        userService.deleteUser(principal.getName());
        return ResponseEntity.ok("User has been deleted");
    }
}
