package com.mix.farmville.models.entities;

import lombok.Data;

import javax.persistence.MappedSuperclass;

@Data
@MappedSuperclass
public class Person {
    private String firstName;
    private String lastName;
    private String address;
    private String phone;
    private String email;
}
