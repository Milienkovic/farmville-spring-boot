package com.mix.farmville.models.entities;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Data
@Entity
@Table(name = "Accounts", uniqueConstraints = {@UniqueConstraint(columnNames = {"account_number"})})
@EntityListeners(AuditingEntityListener.class)
public class Account {
    @Id
    @Column
    private Integer id;
    @Column(name = "account_number")
    private Integer accountNumber;
    @Column
    private Double funds;
    @CreatedDate
    @Column(name = "created_at", updatable = false)
    private Timestamp createdAt;
    @Column(name = "last_modified")
    @LastModifiedDate
    private Timestamp lastModified;
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "account", cascade = CascadeType.ALL)
    private List<Farm> farms;
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToMany(mappedBy = "accounts")
    private List<User> users;
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @MapsId
    @JoinColumn(name = "customer_id")
    @OneToOne(cascade = CascadeType.MERGE)
    private Customer customer;
}
