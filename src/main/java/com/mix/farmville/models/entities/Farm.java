package com.mix.farmville.models.entities;

import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@Entity
@Table(name = "Farms", uniqueConstraints = {@UniqueConstraint(columnNames = {"name"})})
@EntityListeners(AuditingEntityListener.class)
@EqualsAndHashCode
public class Farm {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column
    private String address;
    @Column
    private String name;
    @Column(name = "created_at", updatable = false)
    @CreatedDate
    private Timestamp createdAt;
    @Column(name = "last_modified")
    @LastModifiedDate
    private Timestamp lastModified;
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne
    @JoinColumn(name = "account_id")
    private Account account;
}
