package com.mix.farmville.models.entities;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "Roles", uniqueConstraints = {@UniqueConstraint(columnNames = {"name"})})
public class Role {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column
    private String name;
}
