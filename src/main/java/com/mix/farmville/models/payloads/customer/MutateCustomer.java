package com.mix.farmville.models.payloads.customer;

import com.mix.farmville.models.payloads.account.MutateAccount;
import com.mix.farmville.services.validators.ValidAddress;
import com.mix.farmville.services.validators.ValidEmail;
import com.mix.farmville.services.validators.ValidPersonName;
import com.mix.farmville.services.validators.ValidPhone;
import lombok.Data;

import javax.validation.Valid;

@Data
public class MutateCustomer {
    @ValidPersonName
    private String firstName;
    @ValidPersonName
    private String lastName;
    @ValidEmail
    private String email;
    @ValidAddress
    private String address;
    @ValidPhone
    private String phone;
    private @Valid MutateAccount account;
}
