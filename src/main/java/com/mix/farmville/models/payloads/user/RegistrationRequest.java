package com.mix.farmville.models.payloads.user;

import com.mix.farmville.services.validators.FieldValuesMatch;
import com.mix.farmville.services.validators.ValidEmail;
import com.mix.farmville.services.validators.ValidPassword;
import lombok.Data;

@Data
@FieldValuesMatch(field = "password", matchingField = "confirmPassword")
public class RegistrationRequest {
    @ValidEmail
    private String email;
    @ValidPassword
    private String password;
    @ValidPassword
    private String confirmPassword;
}
