package com.mix.farmville.models.payloads.user;

import com.mix.farmville.services.validators.ValidAddress;
import com.mix.farmville.services.validators.ValidEmail;
import com.mix.farmville.services.validators.ValidPersonName;
import com.mix.farmville.services.validators.ValidPhone;
import lombok.Data;

import javax.validation.constraints.Past;
import java.sql.Timestamp;

@Data
public class UserInfo {
    private Integer id;
    @ValidPersonName
    private String firstName;
    @ValidPersonName
    private String lastName;
    @ValidEmail
    private String email;
    @ValidAddress
    private String address;
    @ValidPhone
    private String phone;
    @Past
    private Timestamp createdAt;
    @Past
    private Timestamp lastModified;
    @Past
    private Timestamp lastVisited;
}
