package com.mix.farmville.models.payloads.user;

import com.mix.farmville.services.validators.ValidAddress;
import com.mix.farmville.services.validators.ValidEmail;
import com.mix.farmville.services.validators.ValidPersonName;
import com.mix.farmville.services.validators.ValidPhone;
import lombok.Data;

@Data
public class MutateUser {
    @ValidPersonName
    private String firstName;
    @ValidPersonName
    private String lastName;
    @ValidPhone
    private String phone;
    @ValidAddress
    private String address;
}
