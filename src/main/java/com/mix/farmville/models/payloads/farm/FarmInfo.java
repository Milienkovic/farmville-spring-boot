package com.mix.farmville.models.payloads.farm;

import com.mix.farmville.services.validators.ValidAddress;
import com.mix.farmville.services.validators.ValidItemName;
import lombok.Data;

import javax.validation.constraints.Past;
import java.sql.Timestamp;

@Data
public class FarmInfo {
    private Integer id;
    @ValidItemName
    private String name;
    @ValidAddress
    private String address;
    @Past
    private Timestamp createdAt;
    @Past
    private Timestamp lastModified;

}
