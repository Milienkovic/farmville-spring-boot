package com.mix.farmville.models.payloads.account;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class AccountInfo {
    private Integer id;
    private Integer accountNumber;
    private Double funds;
    private Timestamp createdAt;
    private Timestamp lastModified;
}
