package com.mix.farmville.models.payloads;

import lombok.Data;

import java.sql.Timestamp;
import java.util.Map;

@Data
public class ErrorResponse {
    private Map<String, String> fieldErrors;
    private String message;
    private Timestamp timestamp;
    private String path;
    private String error;
    private Integer status;
}
