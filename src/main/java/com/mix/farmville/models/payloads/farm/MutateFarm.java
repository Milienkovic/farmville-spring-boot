package com.mix.farmville.models.payloads.farm;

import com.mix.farmville.services.validators.ValidAddress;
import com.mix.farmville.services.validators.ValidItemName;
import lombok.Data;

@Data
public class MutateFarm {
    @ValidItemName
    private String name;
    @ValidAddress
    private String address;
}
