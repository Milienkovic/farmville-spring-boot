package com.mix.farmville.models.payloads.user;

import com.mix.farmville.services.validators.ValidEmail;
import com.mix.farmville.services.validators.ValidPassword;
import lombok.Data;

@Data
public class LoginRequest {
    @ValidEmail
    private String email;
    @ValidPassword
    private String password;
}
