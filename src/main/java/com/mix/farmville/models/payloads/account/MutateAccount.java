package com.mix.farmville.models.payloads.account;

import com.mix.farmville.services.validators.PositiveInteger;
import lombok.Data;

@Data
public class MutateAccount {
    @PositiveInteger
    private Integer accountNumber;
    private Double funds;
}
