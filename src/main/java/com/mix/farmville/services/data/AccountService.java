package com.mix.farmville.services.data;

import com.mix.farmville.models.payloads.account.AccountInfo;
import com.mix.farmville.models.payloads.account.MutateAccount;

import java.util.List;

public interface AccountService {
    List<AccountInfo> findAll();

    AccountInfo findById(Integer accountId);

    AccountInfo updateAccount(Integer accountId, MutateAccount mutateAccount);

    List<AccountInfo> findUserAvailableAccounts(String email);

    void grantAccess(Integer accountId, String email);

    void revokeAccess(Integer accountId, String email);
}

