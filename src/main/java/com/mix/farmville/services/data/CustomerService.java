package com.mix.farmville.services.data;

import com.mix.farmville.models.payloads.customer.CustomerInfo;
import com.mix.farmville.models.payloads.customer.MutateCustomer;

import java.util.List;

public interface CustomerService {
    CustomerInfo findById(Integer customerId);
    List<CustomerInfo> findAll();
    void deleteById(Integer customerID);
    CustomerInfo createCustomer(MutateCustomer mutateCustomer);
    CustomerInfo updateCustomer(Integer customerId, MutateCustomer mutateCustomer);
}
