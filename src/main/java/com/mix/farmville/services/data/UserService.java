package com.mix.farmville.services.data;

import com.mix.farmville.models.payloads.user.MutateUser;
import com.mix.farmville.models.payloads.user.RegistrationRequest;
import com.mix.farmville.models.payloads.user.UserInfo;

import java.util.List;

public interface UserService {
    UserInfo findById(Integer id);
    UserInfo findByEmail(String email);
    List<UserInfo> findAll();
    UserInfo updateUser(Integer userId, MutateUser user);
    void deleteById(Integer userId);
    void deleteUser(String email);
    UserInfo createUser(RegistrationRequest registrationRequest);
}
