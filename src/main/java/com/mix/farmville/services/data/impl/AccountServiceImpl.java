package com.mix.farmville.services.data.impl;

import com.mix.farmville.exceptions.ResourceNotFoundException;
import com.mix.farmville.models.entities.Account;
import com.mix.farmville.models.entities.User;
import com.mix.farmville.models.payloads.account.AccountInfo;
import com.mix.farmville.models.payloads.account.MutateAccount;
import com.mix.farmville.repos.AccountRepo;
import com.mix.farmville.repos.UserRepo;
import com.mix.farmville.services.data.AccountService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class AccountServiceImpl implements AccountService {

    private final AccountRepo accountRepo;
    private UserRepo userRepo;
    private ModelMapper modelMapper;

    public AccountServiceImpl(AccountRepo accountRepo) {
        this.accountRepo = accountRepo;
    }

    @Autowired
    public void setModelMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Autowired
    public void setUserRepo(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @Override
    public List<AccountInfo> findAll() {
        return accountRepo.findAll().stream()
                .map(account -> modelMapper.map(account, AccountInfo.class))
                .collect(Collectors.toList());
    }

    @Override
    public AccountInfo findById(Integer accountId) {
        Account account = getAccount(accountId);
        return modelMapper.map(account, AccountInfo.class);
    }

    @Override
    public AccountInfo updateAccount(Integer accountId, MutateAccount mutateAccount) {
        Account account = getAccount(accountId);
        modelMapper.map(mutateAccount, account);
        return modelMapper.map(accountRepo.save(account), AccountInfo.class);
    }

    @Override
    public List<AccountInfo> findUserAvailableAccounts(String email) {
        User user = userRepo.findByEmail(email).orElseThrow(() -> new ResourceNotFoundException("User not found!"));
        return user.getAccounts().stream()
                .map(account -> modelMapper.map(account, AccountInfo.class))
                .collect(Collectors.toList());
    }

    @Override
    public void grantAccess(Integer accountId, String email) {
        Account account = getAccount(accountId);
        User user = getUser(email);
        user.addAccount(account);
        userRepo.save(user);
    }

    @Override
    public void revokeAccess(Integer accountId, String email) {
        Account account = getAccount(accountId);
        User user = getUser(email);
        user.removeAccount(account);
        userRepo.save(user);
    }

    private Account getAccount(Integer accountId) {
        return accountRepo.findById(accountId).orElseThrow(() -> new ResourceNotFoundException("Account not found!"));
    }

    private User getUser(String email) {
        return userRepo.findByEmail(email).orElseThrow((() -> new ResourceNotFoundException("User not found")));
    }
}
