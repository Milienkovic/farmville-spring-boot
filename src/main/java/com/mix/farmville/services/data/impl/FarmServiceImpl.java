package com.mix.farmville.services.data.impl;

import com.mix.farmville.exceptions.DuplicateEntryException;
import com.mix.farmville.exceptions.ResourceNotFoundException;
import com.mix.farmville.models.entities.Account;
import com.mix.farmville.models.entities.Farm;
import com.mix.farmville.models.entities.User;
import com.mix.farmville.models.payloads.farm.FarmInfo;
import com.mix.farmville.models.payloads.farm.MutateFarm;
import com.mix.farmville.repos.AccountRepo;
import com.mix.farmville.repos.FarmRepo;
import com.mix.farmville.repos.UserRepo;
import com.mix.farmville.services.data.FarmService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
public class FarmServiceImpl implements FarmService {

    private final FarmRepo farmRepo;
    private AccountRepo accountRepo;
    private UserRepo userRepo;
    private ModelMapper modelMapper;

    public FarmServiceImpl(FarmRepo farmRepo) {
        this.farmRepo = farmRepo;
    }

    @Autowired
    public void setModelMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Autowired
    public void setAccountRepo(AccountRepo accountRepo) {
        this.accountRepo = accountRepo;
    }

    @Autowired
    public void setUserRepo(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @Override
    public FarmInfo findById(Integer farmId) {
        return modelMapper.map(getFarm(farmId), FarmInfo.class);
    }

    @Override
    public List<FarmInfo> findAll() {
        return farmRepo.findAll().stream()
                .map(farm -> modelMapper.map(farm, FarmInfo.class))
                .collect(Collectors.toList());
    }

    @Override
    public List<FarmInfo> findUserAvailableFarms(String email) {
        User user = userRepo.findByEmail(email).orElseThrow(() -> new ResourceNotFoundException("User not found"));
        return farmRepo.findUserAccessibleFarms(user.getId())
                .map(farm -> modelMapper.map(farm, FarmInfo.class))
                .collect(Collectors.toList());
    }

    @Override
    public List<FarmInfo> findAccountFarms(Integer accountId) {
        return farmRepo.findAllByAccountId(accountId)
                .map(farm -> modelMapper.map(farm, FarmInfo.class))
                .collect(Collectors.toList());
    }

    @Override
    public void deleteById(Integer farmId) {
        farmRepo.deleteById(farmId);
    }

    @Override
    public FarmInfo createFarm(Integer accountId, MutateFarm mutateFarm) {
        if (farmRepo.existsByName(mutateFarm.getName())) {
            throw new DuplicateEntryException("Farm exists already");
        }
        Account account = getAccount(accountId);
        Farm farm = modelMapper.map(mutateFarm, Farm.class);
        farm.setAccount(account);
        return modelMapper.map(farmRepo.save(farm), FarmInfo.class);
    }

    @Override
    public FarmInfo updateFarm(Integer farmId, MutateFarm mutateFarm) {
        if (farmRepo.existsByName(mutateFarm.getName())) {
            Farm f = getFarm(mutateFarm.getName());
            if (!f.getId().equals(farmId)) {
                throw new DuplicateEntryException("Farm exists already");
            }
        }
        Farm farm = getFarm(farmId);
        modelMapper.map(mutateFarm, farm);
        return modelMapper.map(farmRepo.save(farm), FarmInfo.class);
    }

    private Farm getFarm(Integer farmId) {
        return farmRepo.findById(farmId).orElseThrow(() -> new ResourceNotFoundException("Farm not found!"));
    }

    private Farm getFarm(String name) {
        return farmRepo.findByName(name).orElseThrow(() -> new ResourceNotFoundException("Farm not found!"));
    }

    private Account getAccount(Integer accountId) {
        return accountRepo.findById(accountId).orElseThrow(() -> new ResourceNotFoundException("Account not found!"));
    }
}
