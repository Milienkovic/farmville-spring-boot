package com.mix.farmville.services.data;

import com.mix.farmville.models.entities.Role;

public interface RoleService {
    Role findByName(String name);
}
