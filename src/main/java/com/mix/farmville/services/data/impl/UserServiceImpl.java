package com.mix.farmville.services.data.impl;

import com.mix.farmville.exceptions.DuplicateEntryException;
import com.mix.farmville.models.entities.Role;
import com.mix.farmville.models.entities.User;
import com.mix.farmville.exceptions.ResourceNotFoundException;
import com.mix.farmville.models.payloads.user.MutateUser;
import com.mix.farmville.models.payloads.user.RegistrationRequest;
import com.mix.farmville.models.payloads.user.UserInfo;
import com.mix.farmville.repos.RoleRepo;
import com.mix.farmville.repos.UserRepo;
import com.mix.farmville.services.data.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    private final UserRepo userRepo;
    private final RoleRepo roleRepo;
    private ModelMapper modelMapper;
    private PasswordEncoder passwordEncoder;

    public UserServiceImpl(UserRepo userRepo, RoleRepo roleRepo) {
        this.userRepo = userRepo;
        this.roleRepo = roleRepo;
    }

    @Autowired
    public void setModelMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserInfo findById(Integer id) {
        return modelMapper.map(getUser(id), UserInfo.class);
    }

    @Override
    public UserInfo findByEmail(String email) {
        return modelMapper.map(getUser(email), UserInfo.class);
    }

    @Override
    public List<UserInfo> findAll() {
        return userRepo.findAll().stream()
                .map(user -> modelMapper.map(user, UserInfo.class))
                .collect(Collectors.toList());
    }

    @Override
    public UserInfo updateUser(Integer userId, MutateUser user) {
        User dbUser = getUser(userId);
        modelMapper.map(user, dbUser);
        return modelMapper.map(userRepo.save(dbUser), UserInfo.class);
    }

    @Override
    public void deleteById(Integer userId) {
        userRepo.removeAccountAccessFromUser(userId);
        userRepo.deleteById(userId);
    }

    @Override
    public void deleteUser(String email) {
        userRepo.deleteByEmail(email);
    }

    @Override
    public UserInfo createUser(RegistrationRequest registrationRequest) {
        if (userRepo.existsByEmail(registrationRequest.getEmail())) {
            throw new DuplicateEntryException("User exists already");
        }
        User user = new User();
        Role role = roleRepo.findByName("USER");
        user.setEmail(registrationRequest.getEmail());
        user.setPassword(passwordEncoder.encode(registrationRequest.getPassword()));
        user.setRole(role);
        return modelMapper.map(userRepo.save(user), UserInfo.class);
    }

    private User getUser(String email) {
        return userRepo.findByEmail(email).orElseThrow(() -> new ResourceNotFoundException(User.class.getSimpleName() + " with email:" + email + " not found"));
    }

    private User getUser(Integer id) {
        return userRepo.findById(id).orElseThrow(() -> new ResourceNotFoundException(User.class.getSimpleName() + " with id:" + id + " not found"));
    }

    public boolean isMe(Integer userId, String email) {
        User user = getUser(userId);
        return user.getEmail().equals(email);
    }

}
