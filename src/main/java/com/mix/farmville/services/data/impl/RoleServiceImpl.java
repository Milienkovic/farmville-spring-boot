package com.mix.farmville.services.data.impl;

import com.mix.farmville.models.entities.Role;
import com.mix.farmville.repos.RoleRepo;
import com.mix.farmville.services.data.RoleService;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {
    private final RoleRepo roleRepo;

    public RoleServiceImpl(RoleRepo roleRepo) {
        this.roleRepo = roleRepo;
    }

    @Override
    public Role findByName(String name) {
        return roleRepo.findByName(name);
    }
}
