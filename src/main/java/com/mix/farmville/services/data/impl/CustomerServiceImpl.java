package com.mix.farmville.services.data.impl;

import com.mix.farmville.exceptions.DuplicateEntryException;
import com.mix.farmville.exceptions.ResourceNotFoundException;
import com.mix.farmville.models.entities.Account;
import com.mix.farmville.models.entities.Customer;
import com.mix.farmville.models.payloads.customer.CustomerInfo;
import com.mix.farmville.models.payloads.customer.MutateCustomer;
import com.mix.farmville.repos.AccountRepo;
import com.mix.farmville.repos.CustomerRepo;
import com.mix.farmville.services.data.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepo customerRepo;
    private AccountRepo accountRepo;
    private ModelMapper modelMapper;

    public CustomerServiceImpl(CustomerRepo customerRepo) {
        this.customerRepo = customerRepo;
    }

    @Autowired
    public void setAccountRepo(AccountRepo accountRepo) {
        this.accountRepo = accountRepo;
    }

    @Autowired
    public void setModelMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public CustomerInfo findById(Integer customerId) {
        return modelMapper.map(getCustomer(customerId), CustomerInfo.class);
    }

    @Override
    public List<CustomerInfo> findAll() {
        return customerRepo.findAll().stream()
                .map(customer -> modelMapper.map(customer, CustomerInfo.class))
                .collect(Collectors.toList());
    }

    @Override
    public void deleteById(Integer customerID) {
        Customer customer = getCustomer(customerID);
        accountRepo.removeAccountFromUsers(customer.getAccount().getId());
        customerRepo.deleteById(customerID);
    }

    @Override
    public CustomerInfo createCustomer(MutateCustomer mutateCustomer) {
        if (customerRepo.existsByEmail(mutateCustomer.getEmail())) {
            throw new DuplicateEntryException("Customer already exists");
        }

        Customer customer = modelMapper.map(mutateCustomer, Customer.class);
        if (customer.getAccount() != null) {
            if (accountRepo.existsByAccountNumber(customer.getAccount().getAccountNumber())) {
                throw new DuplicateEntryException("Account already exists");
            }
            customer.getAccount().setCustomer(customer);
        }
        return modelMapper.map(customerRepo.save(customer), CustomerInfo.class);
    }

    @Override
    public CustomerInfo updateCustomer(Integer customerId, MutateCustomer mutateCustomer) {
        if (customerRepo.existsByEmail(mutateCustomer.getEmail())) {
            Customer c = getCustomer(mutateCustomer.getEmail());
            if (!c.getId().equals(customerId)) {
                throw new DuplicateEntryException("Customer already exists");
            }
        }

        if (accountRepo.existsByAccountNumber(mutateCustomer.getAccount().getAccountNumber())) {
            Account account = accountRepo.findByAccountNumber(mutateCustomer.getAccount().getAccountNumber());
            if (!account.getId().equals(customerId)) {
                throw new DuplicateEntryException("Account already exists");
            }
        }
        Customer customer = getCustomer(customerId);
        modelMapper.map(mutateCustomer, customer);
        return modelMapper.map(customerRepo.save(customer), CustomerInfo.class);
    }

    private Customer getCustomer(Integer customerId) {
        return customerRepo.findById(customerId).orElseThrow(() -> new ResourceNotFoundException("Customer not found!"));
    }

    private Customer getCustomer(String email) {
        return customerRepo.findByEmail(email).orElseThrow(() -> new ResourceNotFoundException("Customer not found!"));
    }

    private Account getAccount(Integer accountId) {
        return accountRepo.findById(accountId).orElseThrow(() -> new ResourceNotFoundException("Account not found"));
    }
}
