package com.mix.farmville.services.data;

import com.mix.farmville.models.payloads.farm.FarmInfo;
import com.mix.farmville.models.payloads.farm.MutateFarm;

import java.util.List;

public interface FarmService {
    FarmInfo findById(Integer farmId);

    List<FarmInfo> findAll();

    List<FarmInfo> findUserAvailableFarms(String email);

    List<FarmInfo> findAccountFarms(Integer accountId);

    void deleteById(Integer farmId);

    FarmInfo createFarm(Integer accountId, MutateFarm mutateFarm);

    FarmInfo updateFarm(Integer farmId, MutateFarm mutateFarm);

}
