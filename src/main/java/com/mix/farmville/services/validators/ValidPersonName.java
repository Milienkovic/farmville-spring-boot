package com.mix.farmville.services.validators;

import com.mix.farmville.services.validators.impl.PersonNameValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = PersonNameValidator.class)
public @interface ValidPersonName {
    String message() default "Invalid Name!";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
    int min() default 2;
    int max() default 30;
}
