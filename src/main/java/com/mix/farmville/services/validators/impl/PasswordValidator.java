package com.mix.farmville.services.validators.impl;

import com.mix.farmville.services.validators.ValidPassword;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordValidator implements ConstraintValidator<ValidPassword, String> {
    private String regex = "";

    public void initialize(ValidPassword constraint) {
        regex = constraint.regex();
    }

    public boolean isValid(String password, ConstraintValidatorContext context) {
        if (!regex.isEmpty()) {
            return password.matches(regex);
        } else {
            return false;
        }
    }
}
