package com.mix.farmville.services.validators.impl;

import com.mix.farmville.services.validators.FieldValuesMatch;
import org.springframework.beans.BeanWrapperImpl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class FieldsMatchValidator implements ConstraintValidator<FieldValuesMatch, Object> {
    private String field;
    private String matchingField;

    public void initialize(FieldValuesMatch constraint) {
        field = constraint.field();
        matchingField = constraint.matchingField();
    }

    public boolean isValid(Object obj, ConstraintValidatorContext context) {
        Object fieldValue = new BeanWrapperImpl(obj).getPropertyValue(field);
        Object matchingFieldValue = new BeanWrapperImpl(obj).getPropertyValue(matchingField);
        if (fieldValue != null) {
            return fieldValue.equals(matchingFieldValue);
        } else {
            return matchingFieldValue == null;
        }
    }
}
