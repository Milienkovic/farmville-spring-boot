package com.mix.farmville.services.validators;

import com.mix.farmville.services.validators.impl.ItemNameValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ItemNameValidator.class)
public @interface ValidItemName {
    String message() default "Invalid Name!";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
