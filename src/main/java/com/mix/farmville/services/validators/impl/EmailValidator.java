package com.mix.farmville.services.validators.impl;

import com.mix.farmville.services.validators.ValidEmail;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class EmailValidator implements ConstraintValidator<ValidEmail, String> {
    private String regex = "";

    public void initialize(ValidEmail constraint) {
        regex = constraint.regex();
    }

    public boolean isValid(String email, ConstraintValidatorContext context) {
        if (!regex.isEmpty()) {
            return email.matches(regex);
        } else {
            return false;
        }
    }
}
