package com.mix.farmville.services.validators.impl;

import com.mix.farmville.services.validators.ValidPhone;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PhoneValidator implements ConstraintValidator<ValidPhone, String> {
    private String regex = "";

    public void initialize(ValidPhone constraint) {
        this.regex = constraint.regex();
    }

    public boolean isValid(String phone, ConstraintValidatorContext context) {
        if (!regex.isEmpty()) {
            return phone.matches(regex);
        } else
            return true;
    }
}
