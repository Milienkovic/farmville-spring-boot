package com.mix.farmville.services.validators.impl;

import com.mix.farmville.services.validators.ValidAddress;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class AddressValidator implements ConstraintValidator<ValidAddress, String> {
    public void initialize(ValidAddress constraint) {
    }

    public boolean isValid(String address, ConstraintValidatorContext context) {
        return address.length() >= 2 && Character.isUpperCase(address.charAt(0));
    }
}
