package com.mix.farmville.services.validators.impl;

import com.mix.farmville.services.validators.PositiveInteger;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class IntegerValidator implements ConstraintValidator<PositiveInteger, Integer> {
    public void initialize(PositiveInteger constraint) {
    }

    public boolean isValid(Integer num, ConstraintValidatorContext context) {
        return num >= 0;
    }
}
