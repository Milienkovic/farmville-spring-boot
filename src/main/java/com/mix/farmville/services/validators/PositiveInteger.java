package com.mix.farmville.services.validators;

import com.mix.farmville.services.validators.impl.IntegerValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Constraint(validatedBy = IntegerValidator.class)
public @interface PositiveInteger {
    String message() default "Number is not positive";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
