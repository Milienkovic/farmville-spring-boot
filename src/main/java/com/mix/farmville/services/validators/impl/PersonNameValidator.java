package com.mix.farmville.services.validators.impl;

import com.mix.farmville.services.validators.ValidPersonName;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PersonNameValidator implements ConstraintValidator<ValidPersonName, String> {
    private int min;
    private int max;
    private String regex;
    private String message;

    public void initialize(ValidPersonName constraint) {
        setMin(constraint.min());
        setMax(constraint.max());
        this.regex = "^[A-Z][a-z]{min,max}$";
        this.regex = this.regex.replaceFirst("min", this.min + "");
        this.regex =  this.regex.replaceFirst("max", this.max + "");
        String defaultMessage = constraint.message();
        this.message = defaultMessage + " Must start with uppercase and be between " + this.min + " and " + this.max + " characters";
    }

    public boolean isValid(String name, ConstraintValidatorContext context) {
        if (!isValidName(name)) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(this.message).addConstraintViolation();
            return false;
        }
        return true;
    }

    private boolean isValidName(String name) {
        Pattern pattern = Pattern.compile(this.regex);
        Matcher matcher = pattern.matcher(name);
        return matcher.matches() && name.length() >= this.min && name.length() <= this.max;
    }

    private void setMin(int min) {
        this.min = Math.max(min, 2);
    }

    private void setMax(int max) {
        this.max = Math.min(max, 49);
        if (this.max < this.min) {
            this.max = this.min + 1;
        }
    }
}
