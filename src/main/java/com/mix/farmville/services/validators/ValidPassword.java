package com.mix.farmville.services.validators;

import com.mix.farmville.services.validators.impl.PasswordValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = PasswordValidator.class)
public @interface ValidPassword {
    String message() default "Invalid Password, must contain a number and a uppercase";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
    String regex() default "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{4,8}$";
}
