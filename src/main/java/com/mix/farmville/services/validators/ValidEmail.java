package com.mix.farmville.services.validators;


import com.mix.farmville.services.validators.impl.EmailValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Constraint(validatedBy= EmailValidator.class)
public @interface ValidEmail {
    String message() default "Invalid Email!";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
    String regex() default "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$*";
}
