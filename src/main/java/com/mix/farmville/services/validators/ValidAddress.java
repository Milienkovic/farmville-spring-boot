package com.mix.farmville.services.validators;

import com.mix.farmville.services.validators.impl.AddressValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Constraint(validatedBy = AddressValidator.class)
public @interface ValidAddress {
    String message() default "Invalid Address!";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
