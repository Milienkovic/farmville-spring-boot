package com.mix.farmville.services.validators.impl;

import com.mix.farmville.services.validators.ValidItemName;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ItemNameValidator implements ConstraintValidator<ValidItemName, String> {
    public void initialize(ValidItemName constraint) {
    }

    public boolean isValid(String name, ConstraintValidatorContext context) {
        return name.length() >= 2 && Character.isUpperCase(name.charAt(0));
    }
}
