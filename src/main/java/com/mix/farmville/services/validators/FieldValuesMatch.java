package com.mix.farmville.services.validators;

import com.mix.farmville.services.validators.impl.FieldsMatchValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = FieldsMatchValidator.class)
public @interface FieldValuesMatch {
    String message() default "Field values dont match!";
    Class<?>[] groups() default {};
    String field();
    String matchingField();
    Class<? extends Payload>[] payload() default {};
    @Target(ElementType.TYPE)
    @Retention(RetentionPolicy.RUNTIME)
    @interface List{
        FieldValuesMatch[] values();
    }
}
