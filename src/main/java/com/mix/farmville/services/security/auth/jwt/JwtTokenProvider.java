package com.mix.farmville.services.security.auth.jwt;

import com.mix.farmville.services.security.CustomUserDetailsService;
import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Slf4j
@Component
public class JwtTokenProvider {
    private static final long TOKEN_VALIDITY = 3 * 60 * 60 * 1000;
    private static final String SECRET_KEY = "ahbUC-JjUhUpqAsR_mdhAy1-crbLq7Ae-XmIoJiqhhXGuNTlE_q1NLRQlNPM1_m7IQkaugHeaNeAagqTm2p4qwIvN_4Q53zlmgwOP_oXaMiKRzWLcPDplouaiBR05E2yxz9YWyvIo60S_Vn09DStPvpi5YZrodAEiWV8_3T_pW8qDtBbjecWFya5EXQjCsn8rNjtOQp0zoENc_vUAqT9wmJNz0Zrv0oPuzS2YdsvYcCcNyBIsjAq9yYxJTXw4Yl6MZx7CulGawIYIpexAnn9rRlPCT2DDl96P58xPbfxHOfc_ly0dbS3HQEN3bTRMbpG6XCCyTidGtC0hf_HEj4t8g";
    private static final String BEARER = "Bearer ";
    private static final String PRIVILEGES = "privileges";
    private final CustomUserDetailsService userDetailsService;


    public JwtTokenProvider(CustomUserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    public String createToken(String email, List<String> privileges) {
        Claims claims = Jwts.claims().setSubject(email);
        claims.put(PRIVILEGES, privileges);
        Date now = new Date();
        long expiresIn = now.getTime() + TOKEN_VALIDITY;
        Date expirationDate = new Date(expiresIn);
        return Jwts.builder()
                .setClaims(claims)
                .setExpiration(expirationDate)
                .setIssuedAt(now)
                .signWith(SignatureAlgorithm.HS256, SECRET_KEY)
                .compact();
    }

    public String createBearerToken(String email, List<String> privileges){
        return BEARER + createToken(email, privileges);
    }
    public boolean validateToken(String token) {
        try {
            Jwts.parser().setSigningKey(SECRET_KEY)
                    .parseClaimsJws(token);
            return true;
        } catch (ExpiredJwtException | UnsupportedJwtException | MalformedJwtException | SignatureException | IllegalArgumentException e) {
            e.printStackTrace();
            return false;
        }
    }

    public String resolveToken(String auth) {
        if (auth != null && !auth.isEmpty() && auth.startsWith(BEARER)) {
            return auth.substring(7);
        }
        return null;
    }

    public Authentication getAuthentication(String token) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(getEmail(token));
        final String[] privileges = new String[1];
        getPrivileges(token).ifPresent(o -> {
            privileges[0] = o.toString().replaceAll("\\[","").replaceAll("]","");
        });
        return new UsernamePasswordAuthenticationToken(userDetails,
                null,
                AuthorityUtils.createAuthorityList(privileges[0]));
    }

    private Optional<Object> getPrivileges(String token) {
        Object privileges = Jwts.parser().setSigningKey(SECRET_KEY)
                .parseClaimsJws(token)
                .getBody().get(PRIVILEGES);
        return Optional.of(privileges);
    }

    private String getEmail(String token) {
        try {
            return Jwts.parser().setSigningKey(SECRET_KEY)
                    .parseClaimsJws(token)
                    .getBody()
                    .getSubject();
        } catch (ExpiredJwtException | MalformedJwtException | SignatureException | UnsupportedJwtException | IllegalArgumentException e) {
            e.printStackTrace();
        }
        return null;
    }
}
