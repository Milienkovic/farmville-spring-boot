package com.mix.farmville.services.security.auth.impl;

import com.mix.farmville.exceptions.ResourceNotFoundException;
import com.mix.farmville.models.entities.User;
import com.mix.farmville.models.payloads.user.LoginRequest;
import com.mix.farmville.repos.UserRepo;
import com.mix.farmville.services.data.UserService;
import com.mix.farmville.services.security.auth.AuthService;
import com.mix.farmville.services.security.auth.jwt.JwtTokenProvider;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class AuthServiceImpl implements AuthService {

    private final UserRepo userRepo;
    private final AuthenticationManager authenticationManager;
    private JwtTokenProvider tokenProvider;

    public AuthServiceImpl(UserRepo userRepo, AuthenticationManager authenticationManager) {
        this.userRepo = userRepo;
        this.authenticationManager = authenticationManager;
    }

    @Autowired
    public void setTokenProvider(JwtTokenProvider tokenProvider) {
        this.tokenProvider = tokenProvider;
    }

    @Override
    public String authUserWithEmailAndPassword(LoginRequest loginRequest) {
        User user = userRepo.findByEmail(loginRequest.getEmail())
                .orElseThrow(() -> new ResourceNotFoundException("User not found"));
        List<String> privileges = new ArrayList<>();
        privileges.add(user.getRole().getName());
        String bearerToken = tokenProvider.createBearerToken(user.getEmail(), privileges);
        Authentication authentication = new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword());
        authenticationManager.authenticate(authentication);
        log.error(authentication +" auth");
        log.error(bearerToken +" auth");
        return bearerToken;
    }
}
