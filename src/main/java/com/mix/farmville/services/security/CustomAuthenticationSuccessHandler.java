package com.mix.farmville.services.security;

import com.mix.farmville.exceptions.ResourceNotFoundException;
import com.mix.farmville.models.entities.User;
import com.mix.farmville.repos.UserRepo;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;

@Component
public class CustomAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {
    private final UserRepo userRepo;

    public CustomAuthenticationSuccessHandler(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authentication) throws IOException, ServletException {
        User user = userRepo.findByEmail(authentication.getName()).orElseThrow(() -> new ResourceNotFoundException("User not found"));
        user.setLastVisited(new Timestamp(System.currentTimeMillis()));
        userRepo.save(user);

        clearAuthenticationAttributes(request);
    }
}
