package com.mix.farmville.services.security.auth;

import com.mix.farmville.models.payloads.user.LoginRequest;

public interface AuthService {
    String authUserWithEmailAndPassword(LoginRequest loginRequest);
}
