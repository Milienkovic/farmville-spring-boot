package com.mix.farmville.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.security.SecureRandom;

@Configuration
public class PasswordEncoderConfig {

    private SecureRandom secureRandom = new SecureRandom("FARMville".getBytes());

    @Bean
    public PasswordEncoder getEncoder() {
        return new BCryptPasswordEncoder(10, secureRandom);
    }
}
