CREATE TABLE `Users`
(
    `id`            INT          NOT NULL AUTO_INCREMENT,
    `first_name`    varchar(50),
    `last_name`     varchar(50),
    `address`       varchar(255),
    `email`         varchar(100) NOT NULL UNIQUE,
    `password`      varchar(50) NOT NULL,
    `phone`         varchar(50),
    `created_at`    TIMESTAMP    NULL DEFAULT NULL,
    `last_modified` TIMESTAMP    NULL DEFAULT NULL,
    `role_id`       INT          NOT NULL,
    `last_visited`  TIMESTAMP    NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
);

CREATE TABLE `Accounts`
(
    `id`             INT       NOT NULL AUTO_INCREMENT,
    `account_number` INT       NOT NULL UNIQUE,
    `funds`          FLOAT     NOT NULL DEFAULT '0.0',
    `customer_id`    INT       NOT NULL,
    `last_modified`  TIMESTAMP NULL     DEFAULT NULL,
    `created_at`     TIMESTAMP NULL     DEFAULT NULL,
    PRIMARY KEY (`id`)
);

CREATE TABLE `Users_Accounts`
(
    `user_id`    INT NOT NULL,
    `account_id` INT NOT NULL
);

CREATE TABLE `Farms`
(
    `id`            INT          NOT NULL AUTO_INCREMENT,
    `account_id`    INT          NOT NULL,
    `address`       varchar(255),
    `name`          varchar(50) NOT NULL UNIQUE,
    `created_at`    TIMESTAMP    NULL DEFAULT NULL,
    `last_modified` TIMESTAMP    NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
);

CREATE TABLE `Customers`
(
    `id`            INT          NOT NULL AUTO_INCREMENT,
    `first_name`    varchar(50),
    `last_name`     varchar(50) NOT NULL,
    `phone`         varchar(50),
    `email`         varchar(100) NOT NULL UNIQUE,
    `address`       varchar(255),
    `last_modified` TIMESTAMP    NULL DEFAULT NULL,
    `created_at`    TIMESTAMP    NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
);

CREATE TABLE `Roles`
(
    `id`   INT          NOT NULL AUTO_INCREMENT,
    `name` varchar(50) NOT NULL UNIQUE,
    PRIMARY KEY (`id`)
);

ALTER TABLE `Users`
    ADD CONSTRAINT `Users_fk0` FOREIGN KEY (`role_id`) REFERENCES `Roles` (`id`);

ALTER TABLE `Accounts`
    ADD CONSTRAINT `Accounts_fk1` FOREIGN KEY (`customer_id`) REFERENCES `Customers` (`id`);

ALTER TABLE `Farms`
    ADD CONSTRAINT `Farms_fk0` FOREIGN KEY (`account_id`) REFERENCES `Accounts` (`id`);

ALTER TABLE `Users_Accounts`
    ADD CONSTRAINT `Users_Accounts_fk0` FOREIGN KEY (`user_id`) REFERENCES `Users` (`id`);

ALTER TABLE `Users_Accounts`
    ADD CONSTRAINT `Users_Accounts_fk1` FOREIGN KEY (`account_id`) REFERENCES `Accounts` (`id`);